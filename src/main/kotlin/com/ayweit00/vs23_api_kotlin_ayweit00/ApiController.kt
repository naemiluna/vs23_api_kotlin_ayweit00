package com.ayweit00.vs23_api_kotlin_ayweit00

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@CrossOrigin(origins = ["*"], allowedHeaders = ["*"], methods = [RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE])
@RequestMapping("/api/v1/todos")
@RestController
class ApiController(val service: TodoService) {

    @Operation(summary ="Returns a list of ToDo items")
    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    fun getAllTodos() = service.getAll()

    @Operation(summary ="Returns a ToDo item for a given id")
    @GetMapping("/{id}")
    @ApiResponses(value = [
        ApiResponse(responseCode = "404", description = "No item with given id can be found")
    ])
    @ResponseStatus(HttpStatus.OK)
    fun getTodoById(@PathVariable id: Long) = service.getById(id)

    @Operation(summary ="Creates a ToDo item by name with priority 2")
    @PostMapping("/{name}")
    @ResponseStatus(HttpStatus.CREATED)
    fun createTodoItem(@PathVariable name: String): TodoItem {
        val newItem = TodoItem(todo = name)
        service.create(newItem)
        return newItem
    }
    
    @Operation(summary ="Creates a ToDo item by name and define priority 1 ... 3")
    @PostMapping("/{name}/{prio}")
    @ResponseStatus(HttpStatus.CREATED)
    fun createTodoItemWithPrio(@PathVariable name: String, @PathVariable prio: Int): TodoItem {
        val newItem = TodoItem(todo = name, priority = prio)
        service.create(newItem)
        return newItem
    }

    @Operation(summary ="Gets a ToDo item by id and updates its priority")
    @PutMapping("/prio/{id}/{newprio}")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "The item was updated"),
        ApiResponse(responseCode = "404", description = "No item with given id can be found")
    ])
    fun updatePrioOnTodoItem(@PathVariable id: Long, @PathVariable newprio: Int): TodoItem {
        val newItem = TodoItem(todo = service.getById(id).todo, priority = newprio, id = id)
        service.create(newItem)
        return newItem
    }

    @Operation(summary ="Gets a ToDo item by id and updates its name")
    @PutMapping("/name/{id}/{newname}")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "The item was updated"),
        ApiResponse(responseCode = "404", description = "No item with given id can be found")
    ])
    fun updateNameOnTodoItem(@PathVariable id: Long, @PathVariable newname: String): TodoItem {
        val newItem = TodoItem(todo = newname, priority = service.getById(id).priority, id = id)
        service.create(newItem)
        return newItem
    }

    @Operation(summary ="Creates a ToDo item based on a JSON object")
    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    fun addTodoItem(@RequestBody item:TodoItem): TodoItem {
        service.create(item)
        return item
    }

    @Operation(summary ="Deletes a ToDo item identified by its id")
    @DeleteMapping("/{id}")
    @ApiResponses(value = [
                ApiResponse(responseCode = "404", description = "No item with given id can be found")
            ])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteItem(@PathVariable id: Long) = service.deleteById(id)
}
