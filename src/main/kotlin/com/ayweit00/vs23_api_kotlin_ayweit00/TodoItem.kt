package com.ayweit00.vs23_api_kotlin_ayweit00

import jakarta.persistence.*

@Entity
//@Table(name = "tb_todo")
data class TodoItem(
        val todo: String ="",
        val priority: Int = 2,
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long = 0
)