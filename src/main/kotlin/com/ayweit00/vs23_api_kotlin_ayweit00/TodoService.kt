package com.ayweit00.vs23_api_kotlin_ayweit00

import com.ayweit00.vs23_api_kotlin_ayweit00.TodoItem
import com.ayweit00.vs23_api_kotlin_ayweit00.TodoRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.net.ResponseCache

@Service
class TodoService(val repository: TodoRepository) {
    fun getAll(): List<TodoItem> = repository.findAll()

    fun getById(id: Long): TodoItem = repository.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    fun create(todoItem: TodoItem): TodoItem = repository.save(todoItem)

    fun deleteById(id: Long) {
        if (repository.existsById(id)) {
            repository.deleteById(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }
}