package com.ayweit00.vs23_api_kotlin_ayweit00

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Vs23ApiKotlinAyweit00Application

fun main(args: Array<String>) {
    runApplication<Vs23ApiKotlinAyweit00Application>(*args)
}