package com.ayweit00.vs23_api_kotlin_ayweit00

import com.ayweit00.vs23_api_kotlin_ayweit00.TodoItem
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TodoRepository : JpaRepository<TodoItem, Long>