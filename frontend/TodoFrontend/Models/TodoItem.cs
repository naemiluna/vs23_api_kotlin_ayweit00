namespace TodoFrontend.Models;

public class TodoItem
{
    public string todo { get; set; } = "";
    public int priority { get; set; } = 2;
    public long id { get; set; } = 0;
}
