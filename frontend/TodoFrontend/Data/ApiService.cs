using System.Text;

namespace TodoFrontend.Data;

using TodoFrontend.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;

public class ApiService
{
    private readonly HttpClient _client;

    public ApiService(HttpClient client)
    {
        _client = client;
    }

    public async Task<List<TodoItem>> GetFromApiAsync(string path)
    {
        var response = await _client.GetAsync(path);
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<List<TodoItem>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
    }

    public async Task<HttpResponseMessage> PostToApiAsync(string path, TodoItem item)
    {
        var itemJson = new StringContent(JsonSerializer.Serialize(item), Encoding.UTF8, "application/json");
        var response = await _client.PostAsync(path, itemJson);
        return response;
    }
    
    public async Task<HttpResponseMessage> DeleteTodoAsync(long id)
    {
        var url = $"api/v1/todos/{id}";
        return await _client.DeleteAsync(url);
    }
}
