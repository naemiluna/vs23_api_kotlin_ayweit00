using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using TodoFrontend;
using TodoFrontend.Data;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

// string baseUrl = Environment.GetEnvironmentVariable("BASE_URL");
string baseUrl = "https://8080-naemiluna-vs23apikotlin-06ypirc981b.ws-eu100.gitpod.io/";

builder.Services.AddScoped(sp => new HttpClient 
{ 
    BaseAddress = new Uri(baseUrl) 
});

builder.Services.AddScoped<ApiService>();

await builder.Build().RunAsync();