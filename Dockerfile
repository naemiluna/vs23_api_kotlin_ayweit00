FROM openjdk:17
LABEL maintainer="ayweit00@hs-esslingen.de"
EXPOSE 8080
WORKDIR /opt/app
COPY target/vs23_api_kotlin_ayweit00-0.0.1-SNAPSHOT.jar app.jar
CMD ["java", "-jar", "app.jar"]
